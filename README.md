# Bankan - Api
[Collection postman](Bankan%20Api.postman_collection.json)

Esse é o backend do app Bankan, um aplicativo web que segue o modelo kanban para organizar seu fluxo de trabalho.

<hr/>

Backend desenvolvido em Java com Spring Boot e Jpa.

<hr/>

Alunos: André Prolo, Marco Antonio e Gustavo Unser.