package br.edu.unisep.bankan_api.domain.dto.lane;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LaneDto {

    private Integer id;

    private String title;
}
