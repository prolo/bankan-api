package br.edu.unisep.bankan_api.domain.usecase.card;

import br.edu.unisep.bankan_api.data.repository.CardRepository;
import br.edu.unisep.bankan_api.domain.builder.card.CardBuilder;
import br.edu.unisep.bankan_api.domain.dto.card.UpdateCardDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UpdateCardUseCase {

//    private final UpdateCardValidator validator; TODO: Incluir vaidator de UpdateCard
    private final CardBuilder builder;
    private final CardRepository cardRepository;

    public void execute(UpdateCardDto updateCard) {
        var card = builder.from(updateCard);
        cardRepository.save(card);
    }
}
