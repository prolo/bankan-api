package br.edu.unisep.bankan_api.domain.dto.card;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CardDto {

    private Integer id;

    private String title;

    private String description;

    private Integer listId;

}
