package br.edu.unisep.bankan_api.domain.dto.card;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegisterCardDto {

    private String title;

    private String description;

    private Integer listId;

}
