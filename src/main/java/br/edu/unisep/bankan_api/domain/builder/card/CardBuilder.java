package br.edu.unisep.bankan_api.domain.builder.card;

import br.edu.unisep.bankan_api.data.entity.card.Card;
import br.edu.unisep.bankan_api.data.entity.lane.Lane;
import br.edu.unisep.bankan_api.domain.dto.card.CardDto;
import br.edu.unisep.bankan_api.domain.dto.card.RegisterCardDto;
import br.edu.unisep.bankan_api.domain.dto.card.UpdateCardDto;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class CardBuilder {

    public List<CardDto> from(List<Card> cards) {
        return cards.stream().map(this::from).collect(Collectors.toList());
    }

    public CardDto from(Card card) {
        return new CardDto(
            card.getId(),
            card.getTitle(),
            card.getDescription(),
            card.getLane().getId()
        );
    }

    public Card from(UpdateCardDto updateCard) {

        Card card = from((RegisterCardDto) updateCard);
        card.setId(updateCard.getId());

        return card;
    }

    public Card from(RegisterCardDto registerCard) {
        Card card = new Card();
        card.setDescription(registerCard.getDescription());
        card.setTitle(registerCard.getTitle());

        var lane = new Lane();
        lane.setId(registerCard.getListId());
        card.setLane(lane);

        return card;
    }
}
