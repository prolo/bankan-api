package br.edu.unisep.bankan_api.domain.usecase.card;

import br.edu.unisep.bankan_api.data.repository.CardRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class DeleteCardByIdUseCase {
        private final CardRepository cardRepository;

        public void execute(Integer cardId) {
            cardRepository.deleteById(cardId);
        }
    }