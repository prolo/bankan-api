package br.edu.unisep.bankan_api.domain.usecase.card;

import br.edu.unisep.bankan_api.data.repository.CardRepository;
import br.edu.unisep.bankan_api.domain.builder.card.CardBuilder;
import br.edu.unisep.bankan_api.domain.dto.card.CardDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class FindAllCardsUseCase {

    private final CardRepository cardRepository;
    private final CardBuilder cardBuilder;

    public List<CardDto> execute() {
        var cards = cardRepository.findAll();
        return cardBuilder.from(cards);
    }
}
