package br.edu.unisep.bankan_api.domain.builder.lane;

import br.edu.unisep.bankan_api.data.entity.lane.Lane;
import br.edu.unisep.bankan_api.domain.dto.lane.LaneDto;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class LaneBuilder {

    public List<LaneDto> from(List<Lane> lanes) {
        return lanes.stream().map(
                lane -> new LaneDto(lane.getId(), lane.getTitle())
        ).collect(Collectors.toList());
    }

}