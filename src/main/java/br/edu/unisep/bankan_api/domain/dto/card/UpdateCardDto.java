package br.edu.unisep.bankan_api.domain.dto.card;

import lombok.Data;

@Data
public class UpdateCardDto extends RegisterCardDto {

    private Integer id;

}
