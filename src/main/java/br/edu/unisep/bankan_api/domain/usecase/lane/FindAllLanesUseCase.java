package br.edu.unisep.bankan_api.domain.usecase.lane;

import br.edu.unisep.bankan_api.data.repository.LaneRepository;
import br.edu.unisep.bankan_api.domain.builder.lane.LaneBuilder;
import br.edu.unisep.bankan_api.domain.dto.lane.LaneDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class FindAllLanesUseCase {

    private final LaneRepository laneRepository;
    private final LaneBuilder laneBuilder;

    public List<LaneDto> execute() {
        var lanes = laneRepository.findAll();
        return laneBuilder.from(lanes);
    }
}