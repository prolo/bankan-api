package br.edu.unisep.bankan_api.domain.usecase.card;

import br.edu.unisep.bankan_api.data.repository.CardRepository;
import br.edu.unisep.bankan_api.domain.builder.card.CardBuilder;
import br.edu.unisep.bankan_api.domain.dto.card.CardDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class FindCardByIdUseCase {
//    private final FindCardByIdValidator validator; TODO: Incluir validator de FindById
    private final CardRepository cardRepository;
    private final CardBuilder builder;

    public CardDto execute(Integer cardId) {
        var card = cardRepository.findById(cardId);
        return card.map(builder::from).orElse(null);
    }
}
