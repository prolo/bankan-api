package br.edu.unisep.bankan_api.domain.usecase.card;

import br.edu.unisep.bankan_api.data.repository.CardRepository;
import br.edu.unisep.bankan_api.domain.builder.card.CardBuilder;
import br.edu.unisep.bankan_api.domain.dto.card.RegisterCardDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class RegisterCardUseCase {

//    private final RegisterCardValidator validator; TODO: Incluir validator de RegisterCard
    private final CardBuilder builder;
    private final CardRepository cardRepository;

    public void execute(RegisterCardDto registerCard) {
        var card = builder.from(registerCard);
        cardRepository.save(card);
    }

}
