package br.edu.unisep.bankan_api.data.entity.card;

import br.edu.unisep.bankan_api.data.entity.lane.Lane;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "cards")
public class Card {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "card_id")
    private Integer id;

    @OneToOne
    @JoinColumn(name = "lane_id", referencedColumnName = "lane_id")
    private Lane lane;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;
}
