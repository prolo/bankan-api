package br.edu.unisep.bankan_api.data.repository;

import br.edu.unisep.bankan_api.data.entity.lane.Lane;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LaneRepository extends JpaRepository <Lane, Integer> {
}
