package br.edu.unisep.bankan_api.data.entity.lane;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "lanes")
public class Lane {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "lane_id")
    private Integer id;

    @Column(name = "title")
    private String title;
}
