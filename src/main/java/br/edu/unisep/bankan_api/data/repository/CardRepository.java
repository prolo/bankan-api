package br.edu.unisep.bankan_api.data.repository;

import br.edu.unisep.bankan_api.data.entity.card.Card;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CardRepository extends JpaRepository<Card, Integer> {
}
