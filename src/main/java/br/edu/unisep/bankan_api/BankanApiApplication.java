package br.edu.unisep.bankan_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "br.edu.unisep")
public class BankanApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BankanApiApplication.class, args);
	}

}
