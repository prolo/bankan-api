package br.edu.unisep.bankan_api.controller.card;

import br.edu.unisep.bankan_api.domain.dto.card.CardDto;
import br.edu.unisep.bankan_api.domain.dto.card.RegisterCardDto;
import br.edu.unisep.bankan_api.domain.dto.card.UpdateCardDto;
import br.edu.unisep.bankan_api.domain.usecase.card.*;
import br.edu.unisep.bankan_api.response.DefaultResponse;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/cards")
public class CardController {

    private FindAllCardsUseCase findAllCards;
    private FindCardByIdUseCase findById;
    private RegisterCardUseCase registerCard;
    private UpdateCardUseCase updateCard;
    private DeleteCardByIdUseCase deleteCard;

    @RolesAllowed({"ROLE_ADMIN", "ROLE_CLIENT"})
    @GetMapping
    public ResponseEntity<DefaultResponse<List<CardDto>>> findAll() {
        var cards = findAllCards.execute();

        return cards.isEmpty() ?
                ResponseEntity.noContent().build() :
                ResponseEntity.ok(DefaultResponse.of(cards));
    }

    @RolesAllowed({"ROLE_ADMIN", "ROLE_CLIENT"})
    @GetMapping("/{id}")
    public ResponseEntity<DefaultResponse<CardDto>> findById(@PathVariable("id") Integer id) {
        var card = findById.execute(id);

        return card == null ?
                ResponseEntity.notFound().build() :
                ResponseEntity.ok(DefaultResponse.of(card));
    }

    @RolesAllowed({"ROLE_ADMIN", "ROLE_CLIENT"})
    @PostMapping
    public ResponseEntity<DefaultResponse<Boolean>> save(@RequestBody RegisterCardDto card) {
        registerCard.execute(card);
        return ResponseEntity.ok(DefaultResponse.of(true));
    }

    @RolesAllowed({"ROLE_ADMIN", "ROLE_CLIENT"})
    @PutMapping
    public ResponseEntity<DefaultResponse<Boolean>> update(@RequestBody UpdateCardDto card) {
        updateCard.execute(card);
        return ResponseEntity.ok(DefaultResponse.of(true));
    }

    @RolesAllowed("ROLE_ADMIN")
    @DeleteMapping("/{id}")
    public ResponseEntity<DefaultResponse<Boolean>> delete(@PathVariable("id") Integer id) {
        deleteCard.execute(id);
        return ResponseEntity.ok(DefaultResponse.of(true));
    }
}
