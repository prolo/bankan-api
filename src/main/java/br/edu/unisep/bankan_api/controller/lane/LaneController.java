package br.edu.unisep.bankan_api.controller.lane;

import br.edu.unisep.bankan_api.domain.dto.lane.LaneDto;
import br.edu.unisep.bankan_api.domain.usecase.lane.FindAllLanesUseCase;
import br.edu.unisep.bankan_api.response.DefaultResponse;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.RolesAllowed;
import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/lanes")
public class LaneController {

    private final FindAllLanesUseCase findAllLanes;

    @RolesAllowed({"ROLE_ADMIN", "ROLE_CLIENT"})
    @GetMapping
    public ResponseEntity<DefaultResponse<List<LaneDto>>> findAll() {
        var lanes = findAllLanes.execute();

        return lanes.isEmpty() ?
                ResponseEntity.noContent().build() :
                ResponseEntity.ok(DefaultResponse.of(lanes));
    }
}